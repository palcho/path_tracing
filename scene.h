#ifndef SCENE_HEADER
#define SCENE_HEADER

#include "mtrx.h"

typedef struct Vector Vector;
typedef struct BiVector BiVector;
typedef struct Rotor Rotor;
typedef struct Color Color;
typedef struct Scene Scene;
typedef struct Object Object;
typedef struct Face Face;


struct Vector {
	float x, y, z;
};

struct BiVector {
	float xy, yz, zx;
};

struct Rotor {
	float w, xy, yz, zx;
};

struct Color {
	float r, g, b, a;
};

struct Face {
	short a, b, c;
};

struct Object {
	Vect vertices[14];
	Face faces[24];
	int num_faces;
	float red, green, blue, ka, kd, ks, kt, n;
};


struct Scene {
	Vect eye;
	struct Size { int w, h; } size; // must be power of 2 and square
	float ortho[4];
	Color background;
	float ambient;

	Object lights[4];
	int num_lights;

	int npaths;
	float tonemapping;
	int seed;

	Object objects[16];
	int num_objects;
};

extern const Scene scene;

#endif // SCENE_HEADER
