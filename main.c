#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include <float.h>
#include <SDL2/SDL.h>
#include "mtrx.h"
#include "scene.h"

#define err(...) do { fprintf(stderr, "error: " __VA_ARGS__); exit(-1); } while (0)
#define EPSILON 1e-5
#define INITIAL_FADE 0.8f
#define FADE_RATE 0.5f
#define REFRACTION_INDEX 1.5f
#define CRITICAL_ANGLE 0.729727656f
#define CRITICAL_ANGLE_COSSINE 0.74535599f
enum RayType { SPECULAR, DIFFUSE, REFRACTIVE };

int num_cpu_cores, cache_line_size;
float hdr = 1.0f;
typedef struct HDR_Pixel { float r, g, b; } HDR_Pixel;
volatile HDR_Pixel *hdr_pixels_ptr;
SDL_atomic_t *semaphores;

int path_trace (void *data);
float closest_interception (Object **closest_object, Vect *closest_intercept, Vect *normal, Vect *parallel, const Vect *ray, const Vect *origin);

int main (int argc, char **argv) {
	SDL_Window *window;
	SDL_Renderer *renderer;
	SDL_Texture *texture;
	SDL_Rect rect = { 0, 0, scene.size.w, scene.size.h };
	SDL_Color *pixels;
	int pitch;
	SDL_Event event;
	int result, quit = false;
	//int counter = 0;
	void *unaligned;

	num_cpu_cores = SDL_GetCPUCount();
	cache_line_size = SDL_GetCPUCacheLineSize();
	unaligned = malloc(scene.size.w * scene.size.h * sizeof(HDR_Pixel) + 2 * cache_line_size);
	memset(unaligned, 0, scene.size.w * scene.size.h * sizeof(HDR_Pixel) + 2 * cache_line_size);
	semaphores = (void *)(((uintptr_t)unaligned + (cache_line_size - 1)) & ~(cache_line_size - 1));
	hdr_pixels_ptr = (void *)semaphores + cache_line_size;
	volatile HDR_Pixel (*hdr_pixels)[scene.size.w] = (void *)hdr_pixels_ptr;

	SDL_Thread *thread[num_cpu_cores];
	srand(8547403);

	for (int i = 0; i < num_cpu_cores; i++) {
		SDL_AtomicSet(&semaphores[i+1], 1);
		thread[i] = SDL_CreateThread(path_trace, NULL, (void *)(uintptr_t)i);
	}

	result = SDL_Init(SDL_INIT_VIDEO);
	if (result != 0) err("failed to initialize video: %s\n", SDL_GetError());

	window = SDL_CreateWindow("path tracing", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, scene.size.w, scene.size.h, SDL_WINDOW_SHOWN);
	if (!window) err("failed to create window: %s\n", SDL_GetError());

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (!renderer) err("failed to create renderer: %s\n", SDL_GetError());

	texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_STREAMING, scene.size.w, scene.size.h);
	if (!texture) err("failed to create rgb texture: %s\n", SDL_GetError());

	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_NONE);
	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xff);
	SDL_RenderClear(renderer);


	while (!quit) {
		SDL_Delay(100);

		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT:
				quit = true;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
				case SDLK_q:
				case SDLK_ESCAPE:
					quit = true;
				case SDLK_SPACE:
					SDL_AtomicSet(&semaphores[0], !SDL_AtomicGet(&semaphores[0]));
					break;
				default:
					break;
				}
			default:
				break;
			}
		}

		SDL_LockTexture(texture, &rect, (void **)&pixels, &pitch);
		for (int i = scene.size.h-1; i >= 0; i--) {
			int num_it = SDL_AtomicGet(&semaphores[i*num_cpu_cores / scene.size.h +1]);
			SDL_Color *line_pixels = (void *)pixels + i * pitch;
			//printf("num it = %d\n", num_it);

			for (int j = scene.size.w-1; j >= 0; j--) {
				line_pixels[j] = (SDL_Color) {
					.r = fminf(hdr_pixels[i][j].r * 0xff / num_it, 0xff),
					.g = fminf(hdr_pixels[i][j].g * 0xff / num_it, 0xff),
					.b = fminf(hdr_pixels[i][j].b * 0xff / num_it, 0xff),
					.a = 0xff
				};
			}
		}
		SDL_UnlockTexture(texture);
		SDL_RenderCopy(renderer, texture, NULL, NULL);
		SDL_RenderPresent(renderer);

/*
		if (counter <= scene.size.w * scene.size.h) {
			int x = counter % scene.size.w;
			int y = counter / scene.size.w;
			SDL_Color color = { 0xff, 0xff, 0x00, 0xff }; // yellow
			rect = (SDL_Rect){ x, y, 1, 1 };
			SDL_LockTexture(texture, &rect, (void **)&pixels, &pitch);
			*pixels = color;
			SDL_UnlockTexture(texture);
			SDL_RenderCopy(renderer, texture, NULL, NULL);
			SDL_RenderPresent(renderer);
			counter++;
		}
*/
	}

	free(unaligned);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}


int path_trace (void *data) {
	int first_line, last_line, index;
	index = (int)(uintptr_t)data;
	float x0 = scene.ortho[0], y0 = scene.ortho[1];
	float x1 = scene.ortho[2], y1 = scene.ortho[3];
	float pixel_width = (x1 - x0) / scene.size.w;
	float pixel_height = (y1 - y0) / scene.size.h;
	bool intercepted;
	volatile HDR_Pixel (*hdr_pixels)[scene.size.w] = (void *)hdr_pixels_ptr;
	float dx = 0, dy = 0;

	Vect ray, pixel, closest_intercept;

	first_line = scene.size.h * index / num_cpu_cores;
	last_line = first_line + scene.size.h / num_cpu_cores - 1;
	printf("index %d: first = %d, last = %d, num cores = %d, line size = %d\n", index, first_line, last_line, num_cpu_cores, cache_line_size);
	//SDL_Color color = { 0xff, 0xff, 0x00, 0xff };

	for (int h = 0; ; h++) {
		// for each line of pixel on the window
		for (int i = first_line; i <= last_line; i++) {
			// for each pixel on this line
			for (int j = 0; j < scene.size.w; j++) {
				Object *closest_object;
				intercepted = false; // useless?
				// choose a ray through the pixel
				pixel = (Vect){ x0 + j*pixel_width - pixel_width/2 + dx, y1 - i*pixel_height - pixel_height/2 + dy, 0 };
				Vect origin = pixel;
				ray = unit_vect(sub_vv(pixel, scene.eye));
				float fade = INITIAL_FADE;
				int ray_type;
				do {
					Vect normal, parallel;

					intercepted = closest_interception(&closest_object, &closest_intercept, &normal, &parallel, &ray, &origin) < FLT_MAX;
					if (intercepted && closest_object->ka == 1) { // it's a light
						fade = sqrtf(fade);
						hdr_pixels[i][j].r += fade * closest_object->red;
						hdr_pixels[i][j].g += fade * closest_object->green;
						hdr_pixels[i][j].b += fade * closest_object->blue;
						intercepted = false;
					}


					// at the closest interception get its color, shoot a ray to the light, and another random one
					// todo: random light location
					if (intercepted) {
						float rand0 = rand()/(float)RAND_MAX;
						if (rand0 < closest_object->kd) {
							ray_type = DIFFUSE;
						} else if (rand0 < closest_object->kd + closest_object->ks) {
							ray_type = SPECULAR;
						} else {
							ray_type = REFRACTIVE;
						}

						if (ray_type == DIFFUSE) {
							for (int m = 0; m < scene.num_lights; m++) {
								const Object *light = &scene.lights[m];
								int face_index = rand() % light->num_faces;
								float rand0 = rand(), rand1 = rand(), rand2 = rand();
								float sum = rand0 + rand1 + rand2;
								const Vect *v0 = &light->vertices[light->faces[face_index].a];
								const Vect *v1 = &light->vertices[light->faces[face_index].b];
								const Vect *v2 = &light->vertices[light->faces[face_index].c];
								Vect light_source = mul_sv(1/sum, add_vv(mul_sv(rand0, *v0), add_vv(mul_sv(rand1, *v1), mul_sv(rand2, *v2))));
								Vect light_ray = unit_vect(sub_vv(light_source, closest_intercept));
								float t_light = vect_len(sub_vv(light_source, closest_intercept));
								Vect light_intercept;

								float t = closest_interception(NULL, &light_intercept, NULL, NULL, &light_ray, &closest_intercept);
								
								if (t > t_light) { // why is it inverted?
									hdr_pixels[i][j].r += fade * closest_object->red;
									hdr_pixels[i][j].g += fade * closest_object->green;
									hdr_pixels[i][j].b += fade * closest_object->blue;
								}
							}
						}

						if (ray_type == DIFFUSE) { // diffuse ray
							float rand0 = sqrtf(rand()/(float)RAND_MAX) * PI/2;
							float rand1 = rand()/(float)RAND_MAX * 2*PI;
							//printf("%f %f\n", rand0, rand1);
							Quat qx = quat_from_vect(parallel, rand0);
							Quat qy = quat_from_vect(normal, rand1);
							ray = unit_vect(mul_qviq(mul_qq(qy, qx), normal));
							fade *= FADE_RATE;
						} else if (ray_type == SPECULAR) { // specular ray
							Quat q = quat_from_vect(normal, PI);
							ray = unit_vect(mul_sv(-1, mul_qviq(q, ray)));
							fade *= 0.98f;
						} else { // refractive
							// I assume the object's mesh is volumetric
							bool ray_freed = false;
							for (int u = 0; u < 4 && !ray_freed; u++) {
								if (fabsf(dot(normal, ray) - 1) > EPSILON) {
									Vect inv_ray = mul_sv(-1, ray);
									Quat q = quat_from_vect(cross(normal, inv_ray), acosf(dot(normal, inv_ray)) - asinf(vect_len(cross(normal, ray)) / 1.5f));
									ray = unit_vect(mul_qviq(q, ray));
								} 
								intercepted = closest_interception(&closest_object, &closest_intercept, &normal, &parallel, &ray, &origin) < FLT_MAX;
								if (acos(dot(normal, ray)) < CRITICAL_ANGLE) {
									Quat q = quat_from_vect(cross(normal, ray), acosf(dot(normal, ray)) - asinf(vect_len(cross(normal, ray)) * 1.5f));
									ray = unit_vect(mul_qviq(q, ray));
									ray_freed = true;
								} else {
									Quat q = quat_from_vect(normal, PI);
									ray = unit_vect(mul_sv(-1, mul_qviq(q, ray)));
								}
								fade *= 0.98f;
								origin = closest_intercept;
							}
						}	
						origin = closest_intercept;
					}

				} while (intercepted && fade > 0.05f);
			}
		}

		while (SDL_AtomicGet(&semaphores[0])) {
			SDL_Delay(100);
		}

		SDL_AtomicAdd(&semaphores[index+1], 1);
		printf("thread %d: iteration %d\n", index, h);
		dx = (float)(rand() - RAND_MAX/2) / (float)(RAND_MAX/2) * pixel_width;
		dy = (float)(rand() - RAND_MAX/2) / (float)(RAND_MAX/2) * pixel_height;

	}
	printf("thread %d done\n", index);
	return 0;
}


float closest_interception (Object **closest_object, Vect *closest_intercept, Vect *normal, Vect *parallel, const Vect *ray, const Vect *origin) {
	bool intercepted = false;
	float smallest_t;
	// for each object
	for (int k = 0; k < scene.num_objects; k++) {
		const Object *obj = &scene.objects[k];

		if (obj->num_faces > 0) {
			// for each triangle of the object
			for (int l = 0; l < obj->num_faces; l++) {
				const Vect v0 = obj->vertices[obj->faces[l].a];
				const Vect v1 = obj->vertices[obj->faces[l].b];
				const Vect v2 = obj->vertices[obj->faces[l].c];

				// find the point where it itercepts the triangles' plane
				Vect plane_normal = unit_vect(cross(sub_vv(v1, v0), sub_vv(v2, v0)));
				if (fabsf(dot(plane_normal, *ray)) < EPSILON) continue;
				float t = dot(plane_normal, sub_vv(v0, *origin)) / dot(plane_normal, *ray);
				Vect intercept = add_vv(*origin, mul_sv(t, *ray));

				// check if if it's in front of the camera and if it's the nearest interception so far
				if (intercepted && t > smallest_t || t < EPSILON) continue;
				// if it is, check if the interception is within the triangle
				bool round = dot(cross(sub_vv(v0, v2), sub_vv(intercept, v2)), plane_normal) < 0;
				if (dot(cross(sub_vv(v1, v0), sub_vv(intercept, v0)), plane_normal) < 0 != round) continue;
				if (dot(cross(sub_vv(v2, v1), sub_vv(intercept, v1)), plane_normal) < 0 != round) continue;

				// if it is, save as the closest interception
				if (closest_intercept) *closest_intercept = intercept;
				if (closest_object) *closest_object = obj;
				if (normal) *normal = plane_normal;
				if (parallel) *parallel = unit_vect(sub_vv(v0, v1));
				smallest_t = t;
				intercepted = true;
			}
		} else {
			// it's a sphere 
			const Vect center = obj->vertices[0];
			const float radius = obj->vertices[1].x;

			// find the closest point where it intercepts the sphere
			Vect distance = sub_vv(*origin, center);
			float rd_proj = dot(*ray, distance);
			float delta = rd_proj*rd_proj - dot(distance, distance) + radius*radius;
			if (delta < 0) continue; 
			float t = -rd_proj - sqrtf(delta);
			Vect intercept = add_vv(*origin, mul_sv(t, *ray));
			
			// check if it's the nearest interception so far
			if (intercepted && t > smallest_t || t < EPSILON) continue;

			// if it is, save as the closest interception
			if (closest_intercept) *closest_intercept = intercept;
			if (closest_object) *closest_object = obj;
			if (normal) *normal = unit_vect(sub_vv(intercept, center));
			if (parallel) *parallel = unit_vect(cross(*ray, *normal));
			smallest_t = t;
			intercepted = true; 
		}
	}
	return intercepted ? smallest_t : FLT_MAX;
}
