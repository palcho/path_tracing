#include "scene.h"

const Object left_wall = { // red
	.vertices = {
		{ -3.8220, -3.8416, -16.5900 },
		{ -3.8220, -3.8416, -32.7600 },
		{ -3.8220, 3.8416, -32.7600 },
		{ -3.8220, 3.8416, -16.5900 },
	},
	.faces = { {0,1,2}, {0,2,3} },
	.num_faces = 2,
	.red = 1, .green = 0, .blue = 0, .ka = 0, .kd = 0.5, .ks = 0.5, .kt = 0, .n = 5
};

const Object right_wall = { // green
	.vertices = {
		{ 3.8220, -3.8416, -32.7600 },
		{ 3.8220, -3.8416, -16.5900 },
		{ 3.8220, 3.8416, -16.5900 },
		{ 3.8220, 3.8416, -32.7600 },
	},
	.faces = { {0,1,2}, {0,2,3} },
	.num_faces = 2,
	.red = 0, .green = 1, .blue = 0, .ka = 0, .kd = 0.99, .ks = 0.01, .kt = 0, .n = 5
};

const Object ground = { // white floor
	.vertices = {
		{ 3.8220, -3.8416, -16.5900 },
		{ 3.8220, -3.8416, -32.7600 },
		{ -3.8220, -3.8416, -32.7600 },
		{ -3.8220, -3.8416, -16.5900 },
	},
	.faces = { {0,1,2}, {0,2,3} },
	.num_faces = 2,
	.red = 1, .green = 1, .blue = 1, .ka = 0, .kd = 0.7, .ks = 0.3, .kt = 0, .n = 5
};

const Object back_wall = { // white
	.vertices = {
		{ -3.8220, -3.8416, -32.7600 },
		{ 3.8220, -3.8416, -32.7600 },
		{ 3.8220, 3.8416, -32.7600 },
		{ -3.8220, 3.8416, -32.7600 },
	},
	.faces = { {0,1,2}, {0,2,3} },
	.num_faces = 2,
	.red = 1, .green = 1, .blue = 1, .ka = 0, .kd = 0.99, .ks = 0.01, .kt = 0, .n = 5
};

const Object ceiling = { // white
	.vertices = {
		{ 3.8220, 3.8416, -32.7600 },
		{ 3.8220, 3.8416, -16.5900 },
		{ -3.8220, 3.8416, -16.5900 },
		{ -3.8220, 3.8416, -32.7600 },
	},
	.faces = { {0,1,2}, {0,2,3} },
	.num_faces = 2,
	.red = 1, .green = 1, .blue = 1, .ka = 0, .kd = 0.99, .ks = 0.01, .kt = 0, .n = 5
};

const Object small_block = { // white
	.vertices = {
		{ 0.56, -1.53, -25.8 },
		{ 0.56, -3.8416, -25.8 },
		{ -0.17, -3.8416, -20.67 },
		{ -0.17, -1.53, -20.67 },
		{ 2.82, -1.53, -24.2 },
		{ 2.82, -3.8416, -24.2 },
		{ 2.09, -3.8416, -19.0 },
		{ 2.09, -1.53, -19.0 },
	},
	.faces = {
		{0,1,2}, {0,2,3}, {5,4,7}, {5,7,6}, {0,3,7},
		{0,7,4}, {3,2,6}, {3,6,7}, {0,4,5}, {0,5,1},
	},
	.num_faces = 10,
	.red = 1, .green = 1, .blue = 1, .ka = 0, .kd = 0.00, .ks = 0.10, .kt = 0, .n = 5
};

const Object big_block = { // white
	.vertices = {
		{ -2.73, 0.8000, -29.0 },
		{ -2.73, -3.8416, -29.0 },
		{ -2.02, -3.8416, -24.0 },
		{ -2.02, 0.8000, -24.0 },
		{ -0.56, 0.8000, -30.6 },
		{ -0.56, -3.8416, -30.6 },
		{ 0.15, -3.8416, -25.6 },
		{ 0.15, 0.8000, -25.6 },
	},
	.faces = {
		{0,1,2}, {0,2,3}, {5,4,7}, {5,7,6}, {0,3,7},
		{0,7,4}, {3,2,6}, {3,6,7}, {0,4,5}, {0,5,1},
	},
	.num_faces = 10,
	.red = 1, .green = 1, .blue = 0, .ka = 0, .kd = 0.99, .ks = 0.01, .kt = 0, .n = 5
};

const Object light = {
	.vertices = {
		{ -0.9100, 3.8360, -23.3240 },
		{ -0.9100, 3.8360, -26.4880 },
		{ 0.9100, 3.8360, -26.4880 },
		{ 0.9100, 3.8360, -23.3240 },
	},
	.faces = { {0,1,2}, {0,2,3} },
	.num_faces = 2,
	.red = 1, .green = 1, .blue = 1, .ka = 1, .kd = 0, .ks = 0, .kt = 0, .n = 5
};

const Object sphere =  {
	.vertices = {
		{ -2.8220, -2.8416, -23 }, // center
		{ 1.0 }, // radius
	},
	.num_faces = 0,
	.red = 1, .green = 1, .blue = 1, .ka = 0, .kd = 0.01, .ks = 0.99, .kt = 0, .n = 5 
};

// ka: emitance
// kd: diffuse
// 1-kd: specular

const Scene scene = {
	.eye = { 0.0, 0.0, 5.7 },
	.size = { 512, 512 },
	.ortho = { -1.0, -1.0, 1.0, 1.0 }, // projection plane
	.background = { 0.0, 0.0, 0.0, 1.0 },
	.ambient = 0.5,

	.lights = { light, },
	.num_lights = 1,

	.npaths = 10,
	.tonemapping = 1.0,
	.seed = 9,
	.objects = { left_wall, right_wall, ground, back_wall, ceiling, small_block, big_block, sphere, light },
	.num_objects = 9,
};
