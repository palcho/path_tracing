SRC = main.c scene.c mtrx.c

release: path_tracing
debug: pt_db

path_tracing: $(SRC) makefile
	gcc $(SRC) -O3 -Wall -flto -lSDL2 -lm -Wno-missing-braces -Wno-parentheses -o $@

pt_db: $(SRC) makefile
	gcc $(SRC) -O0 -ggdb -Wall -flto -lm -lSDL2 -Wno-missing-braces -Wno-parentheses -o $@

clean:
	-rm -f path_tracing pt_db
